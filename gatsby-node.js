const path = require('path');

exports.onCreateWebpackConfig = ({
 actions
}) => {
  actions.setWebpackConfig({
    resolve: {
      alias: {
        Components : path.resolve(__dirname, './src/components'),
        Images     : path.resolve(__dirname, './src/images'),
        Pages      : path.resolve(__dirname, './src/pages'),
        Utilities  : path.resolve(__dirname, './src/utilities'),
      }
    },
  })
}