import React, { Component } from 'react';

// Components
import Layout from 'Components/Layout';
import Cart from 'Components/Cart';
import Products from 'Components/Products';

import 'Utilities/state';

// Variables
const purchases = JSON.parse(localStorage.getItem('purchases')) || [];
window.purchases = purchases;

window.state = {
  isCartActive: false
};

class IndexPage extends Component {

  update = () => {
    localStorage.setItem('purchases', JSON.stringify(window.purchases));
    
    // [TODO] Use this one
    // localStorage.setItem('state', JSON.stringify(window.state));

    this.forceUpdate();
  }
  
  handleCartUpdate = ({ index, quantity }) => {
    window.purchases[index].quantity = quantity;

    this.update();
  }

  handlePurchase = ({ name, price }) => {
    const currentItemIndex = window.purchases.findIndex(item => (item.name === name));

    if (currentItemIndex === -1) {
      window.purchases.push({ name, price, quantity: 1 });
    } else {
      window.purchases[currentItemIndex].quantity += 1;
    }

    this.update();
  }

  handleCartToggle = () => {
    const { isCartActive } = window.state;

    window.state.isCartActive = (isCartActive) ? false : true;

    this.update();
  }
  
  render() {
    return (
      <Layout>
        <Products
          onClick={ this.handlePurchase }
        />
        <Cart
          onUpdate={ this.handleCartUpdate }
          onActiveToggle={ this.handleCartToggle }
          purchases={ window.purchases }
          active={ window.state.isCartActive }
        />
      </Layout>
    );
  }
}
export default IndexPage
