import { darken, desaturate } from 'polished';

export const productBlue = '#03c';
export const black = desaturate(0.8, darken(0.3, productBlue));
