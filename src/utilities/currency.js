export const getPrice = (cents) => (cents / 100).toFixed(2);
// export const getPrice = (cents) => (cents / 100).toLocaleString('en-US', { style: 'currency', currency: 'USD' });
