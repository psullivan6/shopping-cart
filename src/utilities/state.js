window.state = {
  isCartActive: false,
  onCartToggle: () => {
    const { isCartActive } = window.state;

    window.state.isCartActive = (isCartActive) ? false : true;
  }
};