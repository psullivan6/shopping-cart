import React from 'react';
import PropTypes from 'prop-types';

// Styles
import {
  Image,
  Container,
  StyledPrice,
  StyledButton,
} from './styles';


const Product = ({ onClick, ...props }) => {
  const { name, price, src } = props;

  return (
    <Container>
      <Image src={ src } alt={ name } />
      
      <StyledPrice cents={ price } />
      
      <StyledButton onClick={ () => onClick({ name, price }) }>Buy</StyledButton>
    </Container>
  )
};

Product.propTypes = {
  onClick : PropTypes.func.isRequired,
  name    : PropTypes.string,
  price   : PropTypes.number,
  src     : PropTypes.string,
};

Product.defaultProps = {
  name    : '',
  price   : 0,
  src     : '',
};

export default Product;
