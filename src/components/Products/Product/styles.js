import styled, { css } from 'styled-components';
import { transparentize } from 'polished';

// Components
import Button from 'Components/Button';
import Price from 'Components/Price';

// Utilities
import { black, productBlue } from 'Utilities/colors';


const largeItem = css`
  @media screen and (min-width: 600px) {
    display: grid;
    grid-column: span 2;
    grid-row: span 2;
  }
`;

export const StyledPrice = styled(Price)`
  position: absolute;
  bottom: -1vmin;
  left: -1vmin;
  margin: 0;
  padding: 1vmin 1.5vmin;
  border-radius: 0.5vmin;
  background-color: ${black};
  color: white;
  transition: bottom 0.2s ease, background-color 0.2s ease;
`;

export const Image = styled.img`
  border-radius: 1vmin;
  transition: opacity 0.3s ease;
`;

export const StyledButton = styled(Button)`
  position: absolute;
  top: 50%;
  left: 50%;
  opacity: 0;
  transform: translate(-50%, 0%);
  border-color: ${productBlue};
  background-color: transparent;
  color: ${productBlue};
  transition: transform 0.3s ease, opacity 0.2s ease;

  &:hover,
  &:focus,
  &:active {
    background-color: ${productBlue};
  }
`;

export const Container = styled.li`
  position: relative;
  border-radius: 1vmin;
  box-shadow 0 0 1em 0 ${transparentize(0.9, black)};
  background-color: white;
  transition: transform 0.1s ease;
  ${props => ([1, 3].indexOf(props.index) !== -1) && largeItem};

  &:hover,
  &:focus,
  &:focus-within {
    transform: scale(1.05);
    box-shadow 0 0 1em 0 ${transparentize(0.8, productBlue)};
    transition: transform 0.5s ease;
  }

  &:hover ${Image},
  &:focus ${Image},
  &:focus-within ${Image} {
    opacity: 0.15;
  }

  &:hover ${StyledButton},
  &:focus ${StyledButton},
  &:focus-within ${StyledButton} {
    opacity: 1;
    transform: translate(-50%, -50%);
    transition: transform 0.8s ease, opacity 0.6s ease;
  }

  &:hover ${StyledPrice},
  &:focus ${StyledPrice},
  &:focus-within ${StyledPrice} {
    bottom: 0;
    background-color: ${productBlue};
    transition: bottom 0.6s ease, background-color 0.6s ease;
  }
`;
