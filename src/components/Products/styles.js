import styled from 'styled-components';

import { cartContainerHeight } from 'Components/Cart/styles';

export const Grid = styled.ul`
  display: grid;
  grid-template-columns: 1fr;
  grid-column-gap: 3vw;
  grid-row-gap: 3vw;
  // align-items: center;
  // grid-auto-rows: minmax(100px, auto);
  margin: ${cartContainerHeight} 0 0 0;
  padding: 0;
  list-style: none;

  @media screen and (min-width: 400px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media screen and (min-width: 600px) {
    grid-template-columns: repeat(3, 1fr);
  }

  img {
    display: block;
    width: 100%;
    margin: auto;
  }
`;
