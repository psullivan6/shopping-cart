import React from 'react';

// Components
import Product from 'Components/Products/Product';

// Images
import shoe from 'Images/shoe.jpg';
import misc from 'Images/misc.jpg';
import phone from 'Images/phone.jpg';
import plants from 'Images/plants.jpg';
import radio from 'Images/radio.jpg';
import succulent from 'Images/succulent.jpg';

// Styles
import { Grid } from './styles';

// Variables
const images = {
  shoe      : { src: shoe, name: 'shoe', key: 'image-shoe', price: 1345 },
  misc      : { src: misc, name: 'misc', key: 'image-misc', price: 441346 },
  phone     : { src: phone, name: 'phone', key: 'image-phone', price: 146 },
  plants    : { src: plants, name: 'plants', key: 'image-plants', price: 8350 },
  radio     : { src: radio, name: 'radio', key: 'image-radio', price: 7843 },
  succulent : { src: succulent, name: 'succulent', key: 'image-succulent', price: 1466 },
};

const Products = ({ onClick }) => {
  return (
    <Grid>
      { 
        Object.values(images).map(({ key, ...props }, index) => <Product key={ key } onClick={ onClick } { ...props } />)
      }
    </Grid>
  )
};

export default Products;
