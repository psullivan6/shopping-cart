import React from 'react';

import { Price, PriceWrapper, Symbol } from './styles';

export default ({ cents, ...props }) => {
  const priceString = (cents / 100).toLocaleString('en-US', { style: 'currency', currency: 'USD' }).split('$')[1];

  return (
    <PriceWrapper { ...props }>
      <Symbol>$</Symbol>
      <Price>{ priceString }</Price>
    </PriceWrapper>
  );
}
