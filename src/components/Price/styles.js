import styled from 'styled-components';

export const PriceWrapper = styled.figure`
  text-align: right;
`;

export const Price = styled.span`
  margin: 0;
  font-family: 'Roboto Mono', monospace;
  font-size: 1rem;
  font-weight: 500;
  line-height: 1;
`;

export const Symbol = styled.span`
  position: relative;
  top: -0.1em;
  margin-right: 0.2em;
  font-size: 0.8em;
  color: #0f9;
`;
