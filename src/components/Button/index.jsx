import React from 'react';
import PropTypes from 'prop-types';

// Styles
import { Button as StyledButton } from './styles';


const Button = ({ children, onClick, ...props }) => (
  <StyledButton
    onClick={ onClick }
    { ...props }
  >
    { children }
  </StyledButton>
);

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default Button;
