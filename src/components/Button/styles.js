import styled from 'styled-components';

import { black } from 'Utilities/colors';

export const Button = styled.button`
  margin: 0;
  padding: 0.6rem 1.2rem;
  border: 2px solid ${black};
  border-radius: 3rem;
  background-color: white;
  font-size: 1rem;
  font-weight: bold;
  line-height: 1;
  letter-spacing: 0.05em;
  text-transform: uppercase;
  transition: background-color 0.1s ease;

  &:hover,
  &:focus {
    cursor: pointer;
    background-color: ${black};
    color: white;
    transition: background-color 0.4s ease;
  }
`;
