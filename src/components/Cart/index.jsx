import React from 'react';

// Components
import Button from 'Components/Button';
import Row from './Row';

// Styles
import {
  CartButton,
  CartIcon,
  Container,
  List,
  TotalIcon,
  TotalRow,
} from './styles';

const getTotalQuantity = (purchases) => (
  purchases.reduce((a, b) => ({
    quantity: a.quantity + b.quantity
  })).quantity
);

const getTotalPrice = (purchases) => {
  let total = 0;

  purchases.forEach(({ price, quantity }) => {
    total += (price * quantity);
  });

  return total;
}

export default ({ onUpdate, purchases, onActiveToggle }) => {
  const { isCartActive } = window.state;
  const totalPurchases = (purchases.length === 0) ? 0 : getTotalQuantity(purchases);
  const totalPrice = (purchases.length === 0) ? 0 : getTotalPrice(purchases);

  let purchasesList = purchases.map(({ name, price, quantity }, index) => (
    (quantity > 0) && (
      <Row
        key={ `Cart-row-${name}` }
        index={ index }
        onUpdate={ onUpdate }
        item={ { name, price } }
        quantity={ quantity }
      />
    )
  ));

  const primaryButton = (!isCartActive)
    ? (
      <CartButton onClick={ onActiveToggle }>
        Shopping Cart
        <CartIcon />
        {
          (totalPurchases > 0) && (
            <TotalIcon>
              { totalPurchases }
            </TotalIcon>
          )
        }
      </CartButton>
    )
    : (
      <Button onClick={ onActiveToggle }>&#10005; Close</Button>
    );

  return (
    <Container active={ isCartActive }>
      { primaryButton }

      <List active={ isCartActive }>
        {
          (totalPurchases > 0)
            ? purchasesList
            : (
              <h1>GO BUY SOMETHING!</h1>
            )
        }
      </List>

      {
        (isCartActive) && (
          <TotalRow
            key={ `Cart-row-total` }
            index={ 0 }
            onUpdate={ onUpdate }
            item={ { name: 'TOTAL', price: totalPrice } }
            quantity={ 1 }
          />
        )
      }
    </Container>
  )
};
