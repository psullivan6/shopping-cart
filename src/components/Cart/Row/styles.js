import styled from 'styled-components';
import { transparentize } from 'polished';

// Components
import CircleButton from 'Components/Cart/CircleButton';
import Price from 'Components/Price';

// Utilities
import { black } from 'Utilities/colors';


export const Row = styled.li`
  box-sizing: border-box;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  padding: 2rem 0.5rem;
  border-bottom: 1px dashed ${transparentize(0.75, 'white')};

  &:last-child {
    border-bottom: none;
  }
`;

export const Name = styled.h1`
  width: 50%;
  font-size: 1.125rem;
`;

export const StyledPrice = styled(Price)`
  display: block;
  width: 11rem;
  margin: 0;
`;

export const RemoveButton = styled(CircleButton)`
  margin-left: 1rem;
  background-color: white;
  color: ${black};

  &:hover,
  &:focus {
    border-color: white;
    background-color: ${black};
    color: white;
  }
`;
