import React, { Component } from 'react';

// Components
import Incrementer from 'Components/Cart/Incrementer';

// Styles
import {
  Name,
  StyledPrice,
  Row as StyledRow,
  RemoveButton,
} from './styles';


export default class Row extends Component {
  update = (event) => {
    const { index, quantity, onUpdate } = this.props;
    let quantityValue;

    if (typeof event === 'string') {
      quantityValue = (event === 'stepUp') ? quantity + 1 : quantity - 1;
    } else {
      quantityValue = event.currentTarget.value;
    }

    onUpdate({ index, quantity: quantityValue });
  }

  render() {
    const { index, item, quantity, onUpdate, ...rest } = this.props;

    return (
      <StyledRow { ...rest }>
        <Name>{ item.name }</Name>

        <Incrementer onChange={ this.update } value={ quantity } />

        <StyledPrice cents={ item.price * quantity } />

        <RemoveButton onClick={ () => { onUpdate({ index, quantity: 0 }) } }>&#10005;</RemoveButton>
      </StyledRow>
    );
  }
}
