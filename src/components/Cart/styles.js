import styled from 'styled-components';
import { transparentize } from 'polished';

// Components
import Button from 'Components/Button';
import Icon from 'Components/Cart/Icon';
import Row from 'Components/Cart/Row';

// Variables
import { black, productBlue } from 'Utilities/colors';

export const cartContainerHeight = '6rem';


export const List = styled.ul`
  overflow: auto;
  flex-grow: 1;
  width: 100%;
  max-width: 1200px;
  margin: 0 auto;
  padding: 0;
`;

export const CartIcon = styled(Icon)`
  fill: ${black};
`;

export const CartButton = styled(Button)`
  position: relative;
  display: flex;
  align-items: center;
  margin: auto;

  &:hover {
    border-color: white;
  }

  &:hover ${CartIcon} {
    fill: #0f9;
  }
`;

export const Container = styled.article`
  z-index: 2;
  box-sizing: border-box;
  overflow: auto;
  display: flex;
  ${({ active }) => (active) && `flex-direction: column`};
  align-items: flex-start;
  position: fixed;
  top: 0;
  left: ${({ active }) => (active) ? '3vmin' : '6vmin' };
  right: ${({ active }) => (active) ? '3vmin' : '6vmin' };
  height: ${({ active }) => (active) ? `calc(100% - 3vmin)` : cartContainerHeight };
  margin: 0;
  padding: ${({ active }) => (active) ? `2rem` : `0` };
  border-radius: 0 0 1vmin 1vmin;
  box-shadow 0 0 1em 0 ${transparentize(0.9, black)};
  background-color: ${black};
  color: white;
  transition: ${({ active }) => (
    `height ${active ? 0.6 : 0.3}s ease,
    left ${active ? 0.6 : 0.3}s ease,
    right ${active ? 0.6 : 0.3}s ease`
  ) };

  ${List} {
    display: ${({ active }) => (active) ? `block` : `none` };
  }

  ${CartButton} {
    display: ${({ active }) => (active && `none`) };
  }
`;


export const TotalIcon = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  top: -1rem;
  right: -1rem;
  width: 2rem;
  height: 2rem;
  border-radius: 50%;
  padding: 0.25rem;
  font-size: 1rem;
  line-height: 1;
  background-color: ${productBlue};
  box-shadow: 0 0 2px 0 rgba(255, 255, 255, 0.5);
  color: white;
`;

export const TotalRow = styled(Row)`
  border-top: 4px solid white;

  // [TODO] Replace with non-hack
  & > div,
  & > button {
    visibility: hidden;
  }
`;