import styled, { css } from 'styled-components';

import CircleButton from 'Components/Cart/CircleButton';

export const Container = styled.div`
  flex-shrink: 0;
`;

export const Number = styled.input`
  appearance: none;
  width: 2em;
  margin: 0.5em;
  padding: 0.25em;
  border: 1px solid #ccc;
  border-radius: 4px;
  font-family: 'Roboto Mono', monospace;
  text-align: center;

  ::-webkit-inner-spin-button, 
  ::-webkit-outer-spin-button { 
    -webkit-appearance: none; 
    margin: 0; 
  }
`;

const Button = css`
  color: white;

  &:hover,
  &:focus {
    background-color: transparent;
  }
`

export const UpButton = styled(CircleButton)`
  ${Button};
  background-color: #063;

  &:hover,
  &:focus {
    border-color: #063;
  }
`;

export const DownButton = styled(CircleButton)`
  ${Button};
  background-color: #903;

  &:hover,
  &:focus {
    border-color: #903;
  }
`