import React from 'react';

// Styles
import { Container, DownButton, UpButton, Number } from './styles';


export default ({ onChange, value }) => (
  <Container>
    <DownButton onClick={ onChange.bind(this, 'stepDown') }>-</DownButton>
    <Number type="number" size="3" value={ value } onChange={ onChange } onInput={ onChange } />
    <UpButton onClick={ onChange.bind(this, 'stepUp') }>+</UpButton>
  </Container>
)