import styled from 'styled-components';

export const StyledIcon = styled.svg`
  display: block;
  width: 1.6rem;
  height: 1.6rem;
  margin: 0 0.5rem 0 0.25rem;
`;
