import styled from 'styled-components';

export const Button = styled.button`
  flex-shrink: 0;
  width: 2rem;
  height: 2rem;
  padding: 0;
  outline: none;
  border: 1px solid transparent;
  border-radius: 50%;
  font-weight: 700;
  line-height: 0.7;
  background: none;
  transform: scale(1);
  transition: border-color 0.1s ease, background-color 0.1s ease, color 0.1s ease, transform 0.4s cubic-bezier(.16,.65,.11,1.06);

  &:hover,
  &:focus {
    cursor: pointer;
    transition: border-color 0.4s ease, background-color 0.4s ease, color 0.4s ease;
  }

  &:active {
    transform: scale(1.1);
    transition: border-color 0.1s ease, background-color 0.1s ease, color 0.1s ease, transform 0.4s cubic-bezier(.16,.65,.11,1.06);
  }
`;
