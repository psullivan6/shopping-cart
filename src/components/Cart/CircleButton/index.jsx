import React from 'react';

import { Button } from './styles';

export default ({ children, ...props }) => (
  <Button { ...props }>{ children }</Button>
);
