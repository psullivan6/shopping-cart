import styled, { createGlobalStyle } from 'styled-components';

// Utilities
import { black } from 'Utilities/colors';

export const GlobalStyle = createGlobalStyle`
  html,
  body {
    width: 100%;
    min-height: 100%;
  }

  body {
    box-sizing: border-box;
    padding: 6vmin;
    font-family: 'Roboto', sans-serif;
    background-color: #F6F6F6;
    color: ${black};
  }
`;

export const Wrapper = styled.section`
  display: flex;
  justify-content: space-between;
`;

export default GlobalStyle;
