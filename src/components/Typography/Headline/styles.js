import styled from 'styled-components';

export const StyledHeadline = styled.h1`
  margin: 0 auto 0.5em auto;
`;

export default {
  StyledHeadline,
};
