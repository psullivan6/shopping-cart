import React from 'react';

// Styles
import { StyledHeadline } from './styles';


export default ({ children }) => (
  <StyledHeadline>{ children }</StyledHeadline>
);
